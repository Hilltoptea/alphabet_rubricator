'''This script will parse your scv file and make a new one with rubricated titles'''

import csv
import re
import dpath

class Manager:
    '''Reads and writes csv files. Make products rubricated'''
    def __init__(self):
        pass
    def make_reader(self,csvfile):
        return self.Reader(csvfile)
    def make_writer(self,csvfile,headers):
        return self.Writer(csvfile,headers)
    def close_file(self,csv_file):
        csv_file.close()
    def rubricate(self,pr_list):
        rub_products = {}
        def make_category_by(product_name):
            return product_name.split(' ')[0]
        for pr in pr_list:
            try:
                dpath.util.new(rub_products, '%s/%s/%s' %(pr.name[0],make_category_by(pr.name),pr.name),pr)
            except Exception as exc:
                print(pr.__dict__)
                raise exc
        return rub_products
    def make_pr_list(self,reader,pr_filter):
        not_valid_values = ['',]
        pr_list = []
        for row in reader:
            row = pr_filter.parse_row(row)
            if row[0] not in not_valid_values and row[1] not in not_valid_values:
                pr_name = row[0]
                pr_attr = row[1]
                pr = Product(pr_name,pr_attr)
                pr_list.append(pr)
        return pr_list

    class Reader:
        '''Open for reading csv file'''
        def __init__(self,csvfile):
            self.csvfile = open(csvfile, newline='',encoding = 'utf-8')
            self.reader = csv.reader(self.csvfile, delimiter = ';')
            self.csv_headers = self.get_headers()
        def get_headers(self):
            return self.reader.__next__()

    class Writer:
        '''Open file for writing data into csv table'''
        def __init__(self,csvfile,headers):
            self.csvfile = open(csvfile, 'w', newline='', encoding = 'utf-8')
            self.writer = csv.writer(self.csvfile, delimiter = ';')
            self.writer.writerow(headers)

class Filter:
    def __init__(self,row,headers,banned_letters = '',filtering_headers=[]):
        def check_(row,headers):
            for header in headers:
                if header not in row:
                    exit('Cannot find "%s" in headers %s' %(header, row))
        check_(row,headers)
        self.headers_indexies = self.__get_indexies(headers,row)
        self.banned_letters = banned_letters
        self.filtering_headers = self.__get_indexies(filtering_headers,headers)
    def __get_indexies(self,names,row):
        iis = []
        for name in names:
            iis.append(row.index(name))
        return iis
    def parse_row(self,row):
        def clip_row(row,indexies):
            clipped = []
            for index in indexies:
                clipped.append(row[index])
            return clipped
        clipped_row = clip_row(row,self.headers_indexies)
        def cut_symbols(banned_letters,string):
            return re.sub('[%s]' %(banned_letters),'',string)
        for ind in self.filtering_headers:
            clipped_row[ind] = cut_symbols(self.banned_letters,clipped_row[ind])
        return clipped_row

class Product:
    def __init__(self,name,attr):
        self.name = name
        self.attr = attr

if __name__ == '__main__':
    import parse_args as args
    parse_headers = ['Название','Артикул']
    filter_headers = ['Название',]
    manager = Manager()
    reader = manager.make_reader(args.input_file)
    pr_filter = Filter(reader.csv_headers,parse_headers,args.banned_letters,filter_headers)
    pr_list = manager.make_pr_list(reader.reader,pr_filter)
    rub_products = manager.rubricate(pr_list)
    manager.close_file(reader.csvfile)
    writer = manager.make_writer(args.output_file,parse_headers)
    writer.writer.writerow(['!Алфавитный указатель',''])
    for letter in sorted(rub_products.keys()):
        writer.writer.writerow(['!!'+letter,''])
        for category in sorted(rub_products[letter].keys()):
            writer.writer.writerow(['!!!'+category,''])
            for pr in sorted(rub_products[letter][category].items()):
                writer.writer.writerow([pr[1].name,pr[1].attr])
    manager.close_file(writer.csvfile)
