import argparse

parser = argparse.ArgumentParser()
parser.add_argument("input", help="input csvfile that would be parsed and rubricated",action="store")
parser.add_argument("-o", "--output", help="output csvfile with rubricated products",action="store")
parser.add_argument("-cut", "--cut", help="Cuts out printed character",action="store")
args = parser.parse_args()

input_file = args.input
if args.output:
    output_file = args.output
else:
    output_file = 'rubricated.csv'
if args.cut:
    banned_letters = args.cut
else:
    banned_letters = '/'
